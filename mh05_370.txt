;0
__main

;7
@Lclearscreenall

;24
@5コーデリアのボーナス

;57
CDR0500840

;68
#ふぅ…。
Hum...

;81
OPR0503340

;92
#どうしたんだい？
What's wrong?

;117
CDR0500850

;128
#あ、教官…。
Ah, Professor...

;147
CDR0500860

;158
#それが…少し考えてしまって…。
Well... I was just thinking...

;204
OPR0503350

;215
#何を？
About?

;225
CDR0500870

;236
#G4の子たちを見ていたら…
#めんどうを見きれる自信が…。
When I look at the people from G4...
I wonder if I can handle them...

;315
OPR0503360

;326
#め、めんどう…？　G4の…？
H-Handle...? G4...?

;365
CDR0500880

;376
#あ、違うんです！
#こ、言葉が足りませんでした！
Ah, no, I mean...
I used the wrong wrongs!

;444
CDR0500890

;455
#その…普段ここにいない子たちを見ていたら…
Well... when I see new faces here...

;519
CDR0500900

;530
#いつかミルキィホームズにも
#新メンバーが加わるんだろうなって…。
It reminds me that one day,
new members might join Milky Holmes.

;625
CDR0500910

;636
#そうなったら私…
#うまくめんどうをみられるかちょっと不安に…。
What that happens, I'm not sure if
I'll be able to watch after them properly...

;728
CDR0500920

;739
#教官はどう思います？
What do you think, Professor?

;770
#大丈夫
You'll be fine

;780
#その時にわかる
You'll know when the time comes

;802
#無理
It's impossible

;809
OPR0503370

;820
#大丈夫…
#キミならきっとうまくやれるさ。
You'll be fine...
If it's you, you will definitely manage.

;879
CDR0500930

;890
#そ、そうですか！？
Really?!

;918
OPR0503380

;929
#その時になったらわかるよ。
You'll know when the time comes.

;969
CDR0500940

;980
#まあ、そうですよね…。
Well, that's true...

;1014
OPR0503390

;1025
#無理じゃないかな…。
It seems unreasonable...

;1056
OPR0503400

;1067
#今のメンバーでさえ
#持てあまし気味だし…。
It feels like even the current
members are too much to handle...

;1129
CDR0500950

;1140
#そうですか…。
I see...

;1162
OPR0503410

;1173
#でもまぁ…
#あまり考えすぎない方がいいんじゃないかな？
But, well, you probably
shouldn't think about it too much.

;1253
CDR0500960

;1264
#あ、はい。
Ah, yes.

;1280
CDR0500970

;1291
#そうですよね…
#今、大事なのは捜査です。
That's right...
We are still in the middle
of an important investigation.

;1350
CDR0500980

;1361
#集中集中！
Concentrate!
Concentrate!

;1377
…………

;1390
CDR0501000

;1401
#く…！
Hm...!

;1411
CDR0501010

;1422
#くぅぅ…！
Hmmm...!

;1438
CDR0501020

;1449
#くく…！
Hmm...!

;1462
OPR0503460

;1473
#わ！！
Wa!!

;1483
OPR0503470

;1494
#コ…コーデリア？
Co...Cordelia?

;1519
CDR0501030

;1530
#ダメです…
#どうしても気になってしまって…。
No good...
It still bothers me...

;1595
OPR0503480

;1606
#そう…。
I see...

;1619
CDR0501040

;1630
――――

;1643
@捜査の後

