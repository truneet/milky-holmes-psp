;0
__main

;7
@Lclearscreenall

;24
@4ネロのキーヒント

;51
TKR0400240

;62
I told you not to enter
the special display room didn't I!?

;145
NER0400440

;156
I know～

;184
OPR0401900

;195
What's wrong?

;220
TKR0400250

;231
Mr Kobayashi!

;247
OPR0401910

;258
Y-Yes!

;274
TKR0400260

;285
I know I'm repeating myself,
but I'm leaving the security to the police!

;371
OPR0401920

;382
Yes... I understand...

;419
TKR0400270

;430
Make sure not to go inside!

;488
OPR0401930

;499
Understood...

;524
TKR0400280

;535
Geeze...!

;554
TKR0400290

;565
What's with this, it's lost its flavour!

;617
OPR0401940

;628
Hm? That thing she just threw away...

;665
NER0400450

;676
Sorry...

;692
OPR0401950

;703
Eh?

;710
NER0400460

;721
You got yelled at too...
I just thought I'd check out things around here...

;828
OPR0401960

;839
And then Miss Takarada got mad at you?

;903
NER0400470

;914
Yeah, it's not like I was even trying
to go into the special display room.

;997
NER0400480

;1008
Tch...

;1021
OPR0401970

;1032
Now now...

;1048
OPR0401980

;1059
So how did it go? Did you find anything?

;1121
NER0400490

;1132
Let's see...

;1151
NER0400500

;1162
What about you, Kobayashi?
Is there anywhere suspicious?

;1223
OPR0401990

;1234
Let's see...

;1253
NER0400540

;1264
Hey.

;1274
OPR0402000

;1285
W-What...?

;1301
NER0400550

;1312
There was nothing wrong with that point.
You just wasted our time.

;1428
OPR0402010

;1439
I see... then...

;1470
4001

;1475
NER0400510

;1486
There? Hmm... Then I'll check it out...

;1557
NER0400560

;1568
A garbage can...?

;1584
OPR0402020

;1595
Yeah, that thing Mr Takarada just threw away...

;1650
NER0400570

;1661
Hey! What're you doing!?

;1692
OPR0402030

;1703
As I thought...

;1722
NER0400580

;1733
What's up?
Suddenly digging through garbage like that...

;1795
OPR0402040

;1806
Do you remember when we first met Miss Takarada?

;1877
NER0400590

;1888
Yeah...

;1901
OPR0402050

;1912
Do you remember her getting angry at Kamitsu?

;1949
NER0400600

;1960
I do.

;1979
OPR0402060

;1990
Do you remember why?

;2021
NER0400610

;2032
Of course.

;2048
OPR0402070

;2059
Then... What is this here,
that Miss Takarada just threw away?

;2124
NER0400620

;2135
Ah!?

;2145
OPR0402080

;2156
Right, this is an important clue...

;2208
@予告の時間

