;0
__main

;7
@Lclearscreenall

;24
@ラット登場

;41
@２章開始用設定

;64
RAT0200010

;75
I see! I've got it, Miss Arsene!

;140
ARS0200010

;151
Ufufu... I'm counting on you, Rat.

;203
RAT0200020

;214
Leave it to me! I'll go all out!

;285
RAT0200030

;296
What a lame town!

;348
RAT0200040

;359
Every one is all carefree, there's no excitement!

;445
RAT0200050

;456
Yeah, that's it!

;478
RAT0200060

;489
Wouldn't it be better
if we just blew everyone away with a bang?

;572
RAT0200070

;583
Instead of doing some small burglaries!
Whaddaya think!?

;648
............

;661
RAT0200080

;672
Miss Arsene, the way you want
to do it isn't very fun anyways! Right!?

;761
YTK0200010

;772
Y-You're being rude! Mr Rat!

;821
YTK0200020

;832
Miss Arsene is thinking
as the leader of the Phantom Thief Empire...

;906
RAT0200090

;917
Ahh～n? What...

;948
RAT0200100

;959
Don't you talk out to me!

;1005
YTK0200030

;1016
Hii...!

;1029
RAT0200110

;1040
Some brat who just follows Miss Arsene around
dares lecturing me, the great Rat...

;1153
RAT0200120

;1164
Just who do you think you are!?

;1210
YTK0200040

;1221
U...Uu...

;1240
ARS0200030

;1251
Rat.

;1264
RAT0200130

;1275
What! Right now I'm...

;1321
ARS0200040

;1332
If you bully Yutaka too much...

;1381
ARS0200050

;1392
I'll punish you.

;1420
RAT0200140

;1431
!!!

;1441
RAT0200150

;1452
I...

;1459
RAT0200160

;1470
I wasn't really... it's just... you know...

;1516
RAT0200170

;1527
Right! It was just a joke! A joke～!

;1579
ARS0200060

;1590
Ufufu... get along with Yutaka,
you are about the same age after all...

;1661
ARS0200070

;1672
Anyway, this operation...

;1703
ARS0200080

;1714
I am leaving it to you.

;1736
RAT0200190

;1747
Yes!

;1760
RAT0200200

;1771
First thing is...

;1799
RAT0200210

;1810
Should I destroy the big building by the coast?
Or maybe that weird shaped hotel...

;1920
RAT0200220

;1931
I know! Destroying all the western style
buildings on the hill sounds fun!

;2005
RAT0200230

;2016
Or maybe the hall in Bashamichi...

;2065
ARS0200090

;2076
RAT0200240

;2087
Hm?

;2094
ARS0200100

;2105
That bad habit of yours,
you can't always be thinking of destroying things.

;2188
ARS0200110

;2199
Especially, hurting people for no reason...

;2257
ARS0200120

;2268
That will definitely not be tolerated.

;2296
RAT0200250

;2307
RAT0200260

;2318
T-This is a joke too～... a joke...

;2367
ARS0200130

;2378
Ufufu... Anyway, I'm leaving this job... to you...

;2446
RAT0200270

;2457
Got it! I'm gonna go now!

;2506
............

;2519
ARS0200150

;2530
*sigh*

;2543
ARS0200160

;2554
What a troublesome child,
even though he is really a good kid...

;2615
MovieOp1

;2624
@中華街の爆発

