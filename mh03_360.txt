;0
__main

;7
@Lclearscreenall

;24
@3コーデリアのキーヒント

;60
OPR0303270

;71
How's it going, Cordelia?

;105
CDR0301290

;116
Professor...

;129
OPR0303275

;140
Have you noticed anything?

;189
CDR0301310

;200
Yes, something about this area is suspicious.

;265
CDR0301320

;276
How about you, Professor? Do you notice anything?

;341
OPR0303280

;352
Let's see...

;371
CDR0301360

;382
Um, I didn't find anything wrong
with that place you showed me...

;510
OPR0303290

;521
I see... Then...

;552
3003

;557
CDR0301330

;568
Understood. I'll examine that place.

;630
CDR0301370

;641
That sticks out, doesn't it?

;681
OPR0303300

;692
Yeah, it's a strange earring...
It's all glass... no, plastic?

;781
CDR0301380

;792
Ah, you don't know? That's called a clear stud.

;881
OPR0303310

;892
Clear stud?

;917
CDR0301390

;928
When you get your ear pierced,
you wear it for a bit so the hole doesn't close...

;1023
CDR0301400

;1034
It's basically a temporary earring.

;1071
OPR0303320

;1082
A temporary earring?

;1107
CDR0301410

;1118
Yes, some people have a metal allergy so...

;1204
CDR0301420

;1215
A special type of resin that has little
affect on those types of people is used.

;1298
OPR0303330

;1309
A temporary earring?

;1331
............

;1344
CDR0301440

;1355
Professor...?

;1368
OPR0303350

;1379
Cordelia... You've found something big...

;1447
CDR0301450

;1458
Sorry?

;1468
OPR0303360

;1479
This is something you use when
you get your ear pierced right?

;1565
CDR0301460

;1576
Yes...

;1589
OPR0303370

;1600
So why is something like that here?

;1665
CDR0301470

;1676
Why... Because the ear was pierced...

;1744
CDR0301480

;1755
Ah!!

;1765
OPR0303380

;1776
Right, this is an important clue!

;1825
@非科学的

