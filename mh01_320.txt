;0
__main

;7
@Lclearscreenall

;24
@010201_ネロのキーヒント

;57
NER0101810

;68
Hm?

;75
OPR0104570

;86
Hey...umm...

;108
NER0101820

;119
Nero's fine.

;141
OPR0104580

;152
Ok...

;162
OPR0104590

;173
Was there something in this area that caught
your attention?

;222
NER0101830

;233
Yeah, something...smells...

;270
OPR0104600

;281
Smells...?

;297
NER0101840

;308
Kind of a lame reaction...Weren't you
supposed to be some great person?

;391
OPR0104610

;402
No, like I said I...

;427
NER0101850

;438
Yes yes, you can't use your Toy right? You're
not a detective right?

;521
NER0101860

;532
I know that ok.

;563
OPR0104620

;574
Geeze...

;593
NER0101870

;604
So? Is there anything you noticed?

;659
OPR0104630

;670
Let's see...

;689
NER0101900

;700
Oi.

;710
OPR0104640

;721
W-What...?

;737
NER0101910

;748
There wasn't anything wrong with the place
you pointed out. We sure wasted our time~

;849
OPR0104650

;860
Is that so...then...

;891
1001

;896
NER0101880

;907
There? Hmm...Then I'll check it out...

;978
NER0101920

;989
Something there...smells...

;1020
OPR0104660

;1031
Yeah, this does smell...

;1074
NER0101930

;1085
A sofa this extravagant and heavy...If it got
knocked over it should make quite some
noise...

;1186
NER0101940

;1197
But we only heard...The sound of the glass
breaking...

;1277
OPR0104670

;1288
It's not just that.

;1316
NER0101950

;1327
Eh?

;1334
OPR0104680

;1345
Go outside the room...To the place where we
heard the glass breaking from.

;1434
NER0101960

;1445
Why?

;1458
OPR0104690

;1469
If you go, you'll see.

;1494
NER0101970

;1505
Ehh...?

;1518
NER0101980

;1529
Is this fine!?

;1551
OPR0104700

;1562
Yeah, I'm going to jump ok!?

;1602
NER0101990

;1613
Jump?

;1626
NER0102000

;1637
!!

;1644
OPR0104710

;1655
How was that!?

;1674
NER0102010

;1685
That's it! This old building is made of wood!

;1731
OPR0104720

;1742
Right, this is an important factor...

;1794
@謎の怪盗

