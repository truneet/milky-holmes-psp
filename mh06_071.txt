;0
__main

;7
@Lclearscreenall

;24
@シャロとポッドの中

;53
CDR0601780

;64
#痛！！　ちょっとネロ！！
#足を動かさないで！！
Ow!! Nero!!
Stop moving your legs!!

;132
NER0601660

;143
#無茶言わないでよ！！
#狭いんだから！！
Come on!!
There's not enough space!!

;199
ERI0601580

;210
#く…苦しい…。
The... suffering...

;232
CDR0601790

;243
#早く外へ出ましょう！
#窒息しちゃうわ！
Let's get out quickly!
I'm going to suffocate!

;299
NER0601670

;310
#それは無理…。
That's impossible...

;332
CDR0601800

;343
#え？　どうして？
Eh? Why?

;368
NER0601680

;379
#水上浮遊中にハッチをひらくことは危険なので
#一度閉めると外側からしか開けられません。
Because opening the hatch
while in water is dangerous,
it can only be opened from the outside.

;504
NER0601690

;515
#って、ここに書いてある…
#救助を待つしかなさそうだよ。
It's written right here...
All we can do is wait, it seems...

;595
CDR0601810

;606
#ええ～！！
Ehh～!!

;622
ERI0601590

;633
#痛い…。
Ow...

;646
NER0601700

;657
#こっちは三人だけど、あっちは二人…
We have 3 people here...
The other pod only has two...

;709
NER0601710

;720
#どうしてるんだか…。
I wonder what they're doing...

;751
…………

;764
OPR0605140

;775
#あ…あの…シャーロック…？
Ah... Um... Sherlock?

;815
OPR0605150

;826
#もう…大丈夫だと思う…よ…？
I think we are safe now...

;869
OPR0605160

;880
#シャーロック…？
Sherlock?

;905
SRK0601760

;916
#怖かった…ですね…。
That was really frightening...

;947
OPR0605180

;958
#あ…うん…。
Yeah...

;977
SRK0601770

;988
#ネロがトイズで
#脱出ポッドを見つけてくれなかったら…
Good thing Nero found
the escape pods with her Toys...

;1065
SRK0601780

;1076
#あたしたちきっと…
Otherwise, we would be...

;1104
OPR0605190

;1115
――――

;1128
SRK0601790

;1139
#でも…
But...

;1149
OPR0605200

;1160
?

;1164
SRK0601800

;1175
#今はうれしいかも…。
I'm kind of happy right now...

;1206
OPR0605210

;1217
!

;1221
SRK0601810

;1232
#先生…。
Teacher...

;1245
OPR0605220

;1256
#はい…？
Yes?

;1269
SRK0601820

;1280
OPR0605230

;1291
#い、いや…
It's... It's nothing...

;1307
OPR0605240

;1318
#その…
Well...

;1328
OPR0605250

;1339
#なんだ…
Um...

;1352
OPR0605260

;1363
#えと…
I mean...

;1373
@エピローグ

