;0
__main

;7
@Lclearscreenall

;24
@洋館の一階へ

;44
OPR0300240

;55
Here eh...

;68
OPR0300250

;79
It's still daytime,
yet there's such a heavy atmosphere...

;138
OPR0300260

;149
I know it's just to prevent crime but,
it's creepy with all the
2nd floor windows boarded up...

;250
OPR0300270

;261
Riku's brother hasn't come out since going in...

;347
OPR0300280

;358
――――

;371
SRK0300220

;382
Th... This is... The rumoured haunted house...

;434
OPR0300290

;445
Eh? Are you scared too?

;482
SRK0300230

;493
I-I'm actually not good with ghosts and stuff...

;539
SRK0300240

;550
Not scary! It's not scary!

;602
ERI0300120

;613
Uuu...

;629
SRK0300250

;640
Eri...! Let's do our best...!

;695
ERI0300130

;706
Y...Yeah, Yeah...!

;734
ALL0300030

;745
Gyaaaa～!!!

;773
NER0300220

;784
Will these two end up being any help?

;845
OPR0300310

;856
Hahaha...

;872
CDR0300100

;883
Professor!

;893
CDR0300110

;904
Sorry to keep you waiting.

;932
OPR0300320

;943
Welcome back, did Riku calm down?

;995
CDR0300120

;1006
Yes, after we reached the academy
he calmed down a bit,
and talked about his brother too.

;1119
CDR0300130

;1130
It seems they had come there
out of curiosity after hearing that rumour.

;1204
CDR0300140

;1215
A treasure hidden in a western house,
protected by ghosts...

;1267
CDR0300150

;1278
A fearsome adventure is
what interests boys that age.

;1385
OPR0300330

;1396
Hm...

;1409
CDR0300160

;1420
Mr Tachi was there so
I asked him to keep Riku company.

;1521
OPR0300340

;1532
I see, that's good.

;1560
CDR0300170

;1571
So then, this is that house.

;1602
OPR0300350

;1613
Yeah... it looks really old...

;1650
NER0300230

;1661
Of course it does.

;1686
NER0300240

;1697
Pretty much all of the western houses
around here were built before the war...

;1783
NER0300250

;1794
Diplomats and important people used them,
but they all left after the war started.

;1889
NER0300260

;1900
Though, after that,
apparently the army used some as well...

;1974
NER0300270

;1985
Now they've become empty houses or tourist spots.

;2065
OPR0300360

;2076
Wow, you know a lot.

;2101
NER0300280

;2112
And there are also cases of ghosts
living in houses like this one...

;2179
SRK0300270

;2190
!

;2194
ERI0300150

;2205
CDR0300180

;2216
Nero, cut it out.

;2259
NER0300290

;2270
Okay～

;2286
CDR0300190

;2297
Well then, Professor, let's go in.

;2334
SRK0300280

;2345
Ehh～!

;2358
ERI0300160

;2369
Uu...!

;2382
NER0300300

;2393
That's unusual of you.

;2418
CDR0300200

;2429
Eh?

;2436
NER0300310

;2447
Normally you'd be like...

;2490
NER0300320

;2501
We can't!

;2514
NER0300330

;2525
There could be dangers we don't know about!
And besides, this is trespassing!

;2614
NER0300340

;2625
Or say something like that.

;2656
CDR0300210

;2667
We don't really have a choice, do we?

;2701
CDR0300220

;2712
In order to find Riku's brother
we have to enter this house.

;2792
CDR0300230

;2803
It'll be too late if we
wait for something to happen.

;2852
NER0300350

;2863
Hmm...

;2879
NER0300360

;2890
Well, I just think it looks interesting,
so I don't really mind.

;2958
NER0300370

;2969
Alright then,
I guess I'll go open the entrance's electric lock.

;3052
NER0300380

;3063
Huh? The indicator is green.

;3118
OPR0300370

;3129
That's right,
because Riku's brother already went in.

;3196
OPR0300380

;3207
It probably wasn't locked.

;3259
NER0300390

;3270
Sure is careless... So, we can go in right?

;3329
CDR0300240

;3340
Yes.

;3353
SRK0300290

;3364
Not scary, not scary!

;3401
ERI0300170

;3412
Not scary... Not scary...!

;3452
NER0300400

;3463
*sigh*

;3476
SRK0300300

;3487
Auu～

;3506
NER0300410

;3517
Ugh, so dusty...

;3551
ERI0300180

;3562
Uu～...

;3578
............

;3591
OPR0300390

;3602
It doesn't look like anyone is here...

;3639
ALL0300040

;3650
!!!

;3660
OPR0300410

;3671
Th-That startled me...

;3693
SRK0300320

;3704
The door closed by itself!!
We've been shut in!!

;3769
ERI0300200

;3780
――――！！

;3799
CDR0300270

;3810
Nero! Did you close it!?

;3865
NER0300430

;3876
I didn't.
Look,

;3907
NER0300440

;3918
maybe it's poorly built?
It closed on it's own.

;3995
CDR0300280

;4006
Oh, so that's what happened.

;4040
NER0300450

;4051
Geeze～
Always suspecting me right away.

;4119
CDR0300290

;4130
I-I'm sorry.

;4161
NER0300460

;4172
Hmph.

;4188
NER0300470

;4199
Hey, Kobayashi?

;4215
OPR0300420

;4226
What is it?

;4242
NER0300480

;4253
This place is pretty big.
Let's split up and look around on this floor.

;4351
SRK0300330

;4362
Ehhh～!!!

;4384
ERI0300210

;4395
Uuu～...!!!

;4420
NER0300490

;4431
Alright alright...
Then you two go with Kobayashi...
Geeze...

;4526
CDR0300300

;4537
I'll go left and look around.

;4568
NER0300500

;4579
Then I'll go right.

;4598
OPR0300430

;4609
Let's go forward.

;4640
SRK0300340

;4651
Ok...

;4664
ERI0300220

;4675
OPR0300440

;4686
Once you've finished searching,
come back here and wait.

;4760
@洋館の二階へ

