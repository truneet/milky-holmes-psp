;0
__main

;7
@Lclearscreenall

;24
@コーデリアとデート

;53
OPR0600090

;64
The prize for solving the case...

;95
OPR0600100

;106
That was the pretext for coming
here with the Milky people, but...

;210
…………

;223
OPR0600120

;234
Maybe I arrived too early...

;265
CDR0600320

;276
#あ…あの…
E-Excuse me...

;292
OPR0603990

;303
!

;307
OPR0604000

;318
#コーデリア…。
Cordelia...

;340
CDR0600340

;351
#お…お待たせしました…。
T-Thank you for waiting.

;388
OPR0604010

;399
#ううん、僕も今来たところだから。
I just arrive myself.

;448
OPR0604020

;459
#あれ、みんなは？　一人だけ？
Ah? What about the others?

;502
CDR0600350

;513
CDR0600360

;524
#そっ…その…み、みみ、みんなは…
#え～と…つ、つまるところ…
W-Well, t-t-the others are...
In short, they...

;613
OPR0604030

;624
#ん？　何かあったの？
Hm? Did something happen?

;655
CDR0600370

;666
#は、はいっ！！
#何かあったんです！！
Y-Yes!!
Something happened!!

;719
OPR0604040

;730
#えっ…！？
Eh?!

;746
CDR0600380

;757
#だから今日は…
#みんな来られないことに～…
Well, the others couldn't come today～...

;819
OPR0604050

;830
#みんな来られない？
#いったい何が？
They couldn't?
What happened?

;880
CDR0600390

;891
#え…え～と…
#ちょ…調子が悪い…らしくて～…
W-Well～...
They weren't feeling well, it seems～...

;956
OPR0604060

;967
#三人とも！？
All three of them?!

;986
CDR0600400

;997
#い、いえ…！
#三人ともというわけでも～…！
N-No...
Not exactly～...

;1059
OPR0604070

;1070
#とにかく連絡してみるよ。
Anyway, I'll try contacting them.

;1107
CDR0600410

;1118
#あ！　ちょっ！　待ってください！
Ah! Please wait!

;1167
OPR0604080

;1178
#どうして？
Why?

;1194
CDR0600420

;1205
#だ、大丈夫ですっ！
#みんな平気ですからっ！
Don't worry!
Everyone is alright!

;1267
OPR0604090

;1278
#は！？
Hm?!

;1288
CDR0600430

;1299
#連絡は…しなくても～…
You don't need to call them～...

;1333
OPR0604100

;1344
#調子が悪いの？　悪くないの？
#いったいどっちなんだい？
Didn't you say they
were not feeling well?

;1424
CDR0600440

;1435
#そ…それは…
W-Well...

;1454
CDR0600450

;1465
#遊びに来られないくらいには…
#調子が悪いというか～…
They just don't feel
well enough to come～...

;1542
CDR0600460

;1553
#教官が気にされるほどでは…
#ないといいますか～…
There's nothing to worry about～...

;1624
OPR0604110

;1635
#おいおい、何かよくわからないな…。
Hey hey, I really don't get it...

;1687
CDR0600470

;1698
#と、とにかく！！
#今日は私と教官のふたりだけなんです！！
A-Anyway!!
Today it's only you and me!!

;1781
OPR0604120

;1792
#うわ！
Wha!

;1802
CDR0600480

;1813
#行きましょう、教官！
Come on, Professor!

;1844
CDR0600490

;1855
#そして、みんなの分まで楽しむんです！
Let's enjoy this day
for everyone's sake!

;1910
CDR0600495

;1921
#いいですね！？
Sounds good?!

;1943
OPR0604130

;1954
#だ、だから、みんなは！？
B-But, what about the others?!

;1991
CDR0600500

;2002
#さて！
Come on!

;2012
CDR0600510

;2023
#まずはどのアトラクションに行きましょう？
Which attraction should we try first?

;2084
OPR0604140

;2095
#あの…コーデリア…？
Um... Cordelia?

;2126
CDR0600520

;2137
#どんなのがお好きですか？
Do you have any preference?

;2174
CDR0600530

;2185
#教官の行きたいところに
#私もお付き合いさせていただいて…
I will accompany you
wherever you decide go...

;2268
CDR0600540

;2279
#あっ、いえ！
Ah, no, I mean...

;2298
CDR0600550

;2309
#お付き合いといっても
#男女の交際のほうの意味では！
Accompany you as a friend of course!

;2383
CDR0600560

;2394
#い、いくらふたりっきりとはいえ…
#そんな…いきなりそこまでは…
E-Even if it is just the two us,
going that far... is too sudden...

;2486
OPR0604150

;2497
#何を言ってるんだ、キミは？
What are you talking about?

;2537
CDR0600570

;2548
#あーーーーっ！！！
Ahーーーー!!!

;2576
OPR0604160

;2587
#ええ！？
Ehh?!

;2600
CDR0600580

;2611
#あれ！　あの乗り物なんかいいですよね！
#教官がお好きそうで！
Look! This attraction looks fun!
I am sure you will like it!

;2700
OPR0604170

;2711
#いや、だから…
As I was saying...

;2733
CDR0600590

;2744
#ほら、行きましょう！
#早く並ばないと！
Come on, let's go!
Hurry up!

;2800
OPR0604180

;2811
#うわ！　押さないで！
Wha! Stop pushing me please!

;2842
CDR0600600

;2853
#あ～おもしろかったですね！
#次はあれに乗りましょう！
Ah～ That was fun!
Let's try this one next!

;2930
OPR0604230

;2941
#コーデリア。
Cordelia.

;2960
CDR0600610

;2971
#今度のも混んでます…
#早く列に並ばないと…
It's crowded again...
We should hurry...

;3033
OPR0604200

;3044
#コーデリア！！
Cordelia!!

;3066
CDR0600620

;3077
OPR0604210

;3088
#ちゃんとみんなのことを話してくれないか？
#ごまかしてばかりいないで。
Would you mind explaining me about the others?
Don't dodge the question, please.

;3189
CDR0600630

;3200
#ご…ごまかすなんて…私は…
I... I didn't really want to lie or anything...

;3240
OPR0604220

;3251
#何があったんだ？
Did something happen?

;3276
CDR0600650

;3287
#ご…
S-

;3294
CDR0600660

;3305
#ごめんなさい…
#私、そんなつもりじゃ…
Sorry...
I didn't have any bad intentions...

;3361
CDR0600670

;3372
#だけど…今日は…
But, today...

;3397
CDR0600680

;3408
#今日はこのまま…。
Today is...

;3436
CDR0600690

;3447
――――

;3460
OPR0604260

;3471
#わかった。
I get it.

;3487
CDR0600710

;3498
#え…？
Eh?

;3508
OPR0604270

;3519
#無理に話さなくてもいいよ。
Don't force yourself.

;3559
OPR0604280

;3570
#キミがそう判断したことなら
#僕はそれを信じる。
If you insist that everything 
is all right, then I believe you.

;3638
OPR0604290

;3649
#みんなに何かあったなら
#キミがここにいるはずないからね。
You wouldn't be here if something
bad happened to the others, right?

;3732
OPR0604300

;3743
#それは間違いない。
I have no doubt about it.

;3771
CDR0600720

;3782
#教官…。
Professor...

;3795
OPR0604310

;3806
#だから、そんな顔をしないでくれ。
So don't make such a face, please.

;3855
CDR0600730

;3866
#あ…ありがとう…ございます…。
T-Thank you... very much...

;3912
OPR0604320

;3923
#さぁ、次のに…
#あれに乗るんだったよね？
What's next...
Didn't you want to try this one here?

;3982
CDR0600740

;3993
#いえ…。
No...

;4006
OPR0604330

;4017
#え？
Eh?

;4024
CDR0600750

;4035
#もし、教官が…
#お嫌でなければ…あれに…。
Could we ride this one?
If you don't mind...

;4097
CDR0600760

;4108
#うふふふふ…！　きょうか～ん…！
Ahahaha! Professor～!

;4157
OPR0604340

;4168
#ははははは…！　コーデリア～…！
Hahahahaha! Cordelia～!

;4217
CDR0600770

;4228
#私…幸せです～…！
I'm... so happy～!

;4256
OPR0604350

;4267
#僕もだよ～…！
My too～!

;4289
CDR0600780

;4300
#うふふふふ…！
Hahaha!

;4322
OPR0604360

;4333
#ははははは…！
Hahahahaha!

;4355
CDR0600790

;4366
OPR0604370

;4377
#ちょ、ちょっと…恥ずかしかったかな…。
That was a bit embarrassing...

;4435
CDR0600800

;4446
#すみません…
#無理につきあってもらって…。
Sorry for forcing this on you...

;4508
OPR0604380

;4519
#あ、いや…
#楽しんでくれたなら問題ないよ。
Don't worry...
It's okay as long as we are having fun.

;4581
CDR0600810

;4592
#夢だったんです…。
I had a dream...

;4620
OPR0604390

;4631
CDR0600820

;4642
#こうして…
#メリーゴーランドに乗るのが。
About riding the Merry Gorando.

;4701
OPR0604400

;4712
#乗ったことなかったの？
You've never been on a Ferris wheel?

;4746
CDR0600830

;4757
#いえ…そうじゃなくて…
That's not it...

;4791
OPR0604410

;4802
?

;4806
CDR0600840

;4817
#その…あの…つ、つまり…
Well... I mean...
In other words...

;4854
CDR0600850

;4865
#だ…男性の方と…
#い…一緒に…乗るのが…
F-Ferris wheel...
W-With a man...

;4924
OPR0604420

;4935
#ああ、そうだったんだ。
Ah, so that was it...

;4969
CDR0600860

;4980
!

;4996
OPR0604430

;5007
#じゃあ、なんだか悪いことしちゃったな。
That's a problem, indeed.

;5065
CDR0600870

;5076
#えっ！？
Eh?!

;5089
OPR0604440

;5100
#相手が僕なんかで…
#キミの意中の人だったらよかったのに…。
It certainly would be better
to go there with someone you love...

;5186
CDR0600880

;5197
#い、いえ！　いいんです！　これで！
N-No! It's alright!

;5249
OPR0604450

;5260
CDR0600890

;5271
#だから…わ…私は…
Because... I...

;5299
CDR0600900

;5310
#教官が…
Like...

;5323
CDR0600910

;5334
#その…。
I mean...

;5347
OPR0604460

;5358
CDR0600920

;5369
#も、もう！
#いつもの洞察力はどこにいったんですか！？
Oh Come on!
Where is your usual insight when you need it?!

;5446
OPR0604470

;5457
#はい？
Sorry?

;5467
CDR0600930

;5478
#次はあれに乗ります！　さぁ行きましょう！
Let's do this next!
Come on, let's go!

;5539
OPR0604490

;5550
#何か悪いことしたかな？
I wonder if I did something wrong?

;5584
CDR0600950

;5595
#教官…
Professor...

;5605
OPR0604510

;5616
#ん…？
Hm?

;5626
CDR0600960

;5637
#今日は…本当にありがとうございました。
Thank you very much for today.

;5695
CDR0600970

;5706
#教官と二人でこんなところに来られるなんて…
#夢みたいです…。
Coming here with you
somehow feels like a dream...

;5795
OPR0604520

;5806
#ハハ…おおげさだって。
Haha...
That's an exaggeration.

;5840
OPR0604530

;5851
#近いんだし…
#ヒマがあったらいつでもつきあうから。
I know what you mean, though...
I'd stick with you every time, if I could.

;5925
CDR0600980

;5936
#本当ですか！！！
Really?!!!

;5961
OPR0604540

;5972
#あ、ああ…。
Ah, yes...

;5991
CDR0600990

;6002
#あ…
Ah...

;6009
CDR0601000

;6020
#す、すみません…私…
#まいあがっちゃって…。
S-Sorry...
I got carried away...

;6085
OPR0604550

;6096
#いや…よろこんでもらえるなら…
#本当にいくらでも…。
No... If it makes you happy,
we can come here whenever you want...

;6173
CDR0601020

;6184
OPR0604570

;6195
#なんだい？
What is it?

;6211
CDR0601030

;6222
#教官は…どうしてそんなに…
#優しいんですか？
Professor, why are you so kind?

;6287
OPR0604580

;6298
#優しい？　そうかな？
Kind? Am I?

;6329
CDR0601040

;6340
#私が失敗しても…
#いつも笑って許してくれる…
Even when I fail, you always
laugh it off and forgive me...

;6405
CDR0601050

;6416
#初めてお会いした時だって…
#あんなことをしてしまったのに…
Even after what I did
when we first met...

;6502
CDR0601060

;6513
#怒りもしないで…
#笑顔でいてくれて…
You didn't get angry
and smiled instead...

;6566
OPR0604590

;6577
#ハハ…そんなこともあったね…。
Haha... I remember that...

;6623
CDR0601070

;6634
#今日もそう…
And today too...

;6653
CDR0601080

;6664
#私を信じてくれて…
You believed me...

;6692
CDR0601090

;6703
#みんなが来ない理由を…
#聞かないでいてくれました…。
You didn't insist on hearing
the reason why the others couldn't come...

;6780
CDR0601100

;6791
#私…
I...

;6798
CDR0601110

;6809
#教官みたいになりたいです。
#教官みたいに大人に。
I want to become like you.
An adult like you.

;6880
OPR0604610

;6891
CDR0601120

;6902
OPR0604620

;6913
#もっと肩の力をぬいて。
You should take it easy.

;6947
CDR0601130

;6958
#肩の…。
Easy...

;6971
OPR0604630

;6982
#キミは十分にがんばってる。
#でも力みすぎなところがあると思う。
You are already doing good enough.
But sometimes, you push yourself too much.

;7074
CDR0601140

;7085
#そ…そうでしょうか…？
Is that so?

;7119
OPR0604640

;7130
#みんなキミのことを頼りにしてるよ。
#もちろん僕も。
Everyone is relying on you.
Including me.

;7204
CDR0601150

;7215
#そ、そんな…
#私なんて…まだまだ…。
But...
I still have a long way to go...

;7268
OPR0604650

;7279
#見てくれている人は
#ちゃんと見てくれている…
People who want to see, will notice...

;7344
OPR0604660

;7355
#だからそのままのキミで
#いいんじゃないかな？
Aren't you good the way you are?

;7420
CDR0601160

;7431
#そのままの…
The way I am...

;7450
OPR0604670

;7461
#うん。
Yeah...

;7471
CDR0601170

;7482
CDR0601180

;7493
#そう言ってもらえて…
#本当にうれしいです…。
I am really happy
after hearing that...

;7558
CDR0601190

;7569
#その包容力…深い優しさ…。
That open-mindedness
of yours is pure kindness...

;7609
CDR0601200

;7620
#やっぱり私…
#教官のようになりたい…。
It's as I thought...
I really want to become like you...

;7676
OPR0604690

;7687
#ハハ…照れくさいな…。
Haha...
That's embarrassing...

;7721
CDR0601210

;7732
#私…！
I...!

;7742
OPR0604700

;7753
CDR0601220

;7764
#教官の…！
Professor...!

;7780
CDR0601230

;7791
#いえ！
No!

;7801
CDR0601240

;7812
#あ、あなたの…
#あなたの…ことが…
K-Kobayashi... I...

;7862
CDR0601250

;7873
#す…すすっ…す…
L...Lo...L...

;7898
ALL0600090

;7909
OPR0604720

;7920
What was that light just now?!

;7954
CDR0601270

;7965
Professor!! Look!!

;7993
CDR0601280

;8004
A fire? An explosion?

;8032
OPR0604730

;8043
That's the Yokohama power plant...

;8101
CDR0601290

;8112
Ah!

;8122
OPR0604740

;8133
The Ferris wheel stopped?!
A power outage?!

;8179
OPR0604750

;8190
As I thought, the power plant...!

;8224
OPR0604760

;8235
!!!

;8245
CDR0601300

;8256
W-What is this sound?

;8293
CDR0601310

;8304
#教官…どうしましょう…。
Professor, what should we do?

;8341
CDR0601320

;8352
Professor...?

;8365
OPR0604790

;8376
The beginning...

;8386
CDR0601330

;8397
OPR0604800

;8408
The bells announcing the beginning...

;8442
CDR0601340

;8453
The beginning... of what...

;8493
OPR0604810

;8504
This sound...
How can I forget forget it...

;8566
CDR0601350

;8577
E-Excuse me...

;8593
OPR0604820

;8604
But that's....
Don't tell me...

;8635
CDR0601360

;8646
What's going on, Professor?

;8683
ELU0600010

;8694
Hahahahaha!

;8713
CDR0601370

;8724
OPR0604830

;8735
N-No way!!

;8760
ELU0600020

;8771
Greetings, ladies and gentlemen...
First, allow me to introduce myself...

;8878
ELU0600030

;8889
My name is L...

;8908
ELU0600040

;8919
Phantom L!!

;8935
CDR0601380

;8946
L!?

;8956
ELU0600050

;8967
And now, let us begin our splendid performance!

;9013
ELU0600060

;9024
The greatest show in history!
The first and probably the last!

;9070
ELU0600070

;9081
In 6 hours from now...

;9115
ELU0600080

;9126
This district will disappear!!!

;9160
CDR0601390

;9171
ELU0600090

;9182
Nothing is impossible for Phantom Thieves!
Phantom Thieves are the
embodiments of God's miracles!

;9259
ELU0600100

;9270
My miracle...

;9298
ELU0600110

;9309
Ladies and gentlemen!
Your eyes, your ears, and your soul...

;9367
ELU0600120

;9378
Enjoy it with everything you have!

;9433
ELU0600130

;9444
Huhuhu...

;9466
ELU0600140

;9477
Haahahahaha!!!

;9529
CDR0601400

;9540
P-Professor...?

;9562
CDR0601410

;9573
That Phantom L...
5 years ago, Teacher was...

;9619
OPR0604850

;9630
N-No way...
This is... impossible...

;9686
OPR0604860

;9697
Because Phantom L...

;9713
OPR0604870

;9724
That time, I'm sure he...

;9764
OPR0604880

;9775
CDR0601420

;9786
My PDA...

;9805
CDR0601430

;9816
It's a direct call...

;9847
OPR0604890

;9858
Pick it up!

;9883
CDR0601440

;9894
Yes!

;9904
SRK0601560

;9915
#コーデリアさん！？
Cordelia?!

;9943
CDR0601450

;9954
#シャロ！？
Sharo?!

;9970
SRK0601570

;9981
Eri will now rotate the Ferris wheel!
Be careful!

;10058
CDR0601460

;10069
Eh?! Eh?! Rotate the wheel?!
Eri will?!

;10134
ALL0600100

;10145
Wha!

;10152
CDR0601480

;10163
W-We are moving!
Eri is turning the wheel?!

;10222
SRK0601580

;10233
#はい、そうです！
#ゴンドラのナンバーはいくつですか！？
Yes! What's your gondola number?!

;10313
CDR0601490

;10324
#ゴンドラのナンバー！？
The gondola number?!

;10358
SRK0601590

;10369
#どこかに書いてあるはすですから
#何番に乗ってるか教えてください！？
It should be written somewhere,
please tell me which one you are in!

;10467
CDR0601500

;10478
Um, let me see...

;10497
CDR0601510

;10508
59! Number 59!

;10531
SRK0601600

;10542
#わかりました！
Understood!

;10564
CDR0601520

;10575
Ah, Sharo, wait! Sharo?!

;10624
CDR0601530

;10635
CDR0601540

;10646
#教官、私なにがなんだか…！
Professor, I don't really
understand what's going on...

;10686
OPR0604910

;10697
Calm down, Cordelia.

;10737
CDR0601550

;10748
Y-Yes...

;10767
OPR0604920

;10778
You need to calm down too!
Kobayashi Opera!

;10830
OPR0604930

;10841
The broadcast from before...

;10866
OPR0604940

;10877
I wonder if it's true...
Is it really Phantom L?

;10939
OPR0604950

;10950
The district will disappear in 6 hours, he said...

;11005
OPR0604960

;11016
Even if it isn't the real L...

;11059
OPR0604970

;11070
What's the meaning behind this?!

;11092
CDR0601560

;11103
CDR0601570

;11114
CDR0601580

;11125
I feel like something
really bad will happen...

;11214
OPR0604980

;11225
Don't worry...

;11238
OPR0604990

;11249
Everything will be fine...

;11271
ERI0601500

;11282
#コーデリアさん…大丈夫ですか…？
Are you alright, Cordelia?

;11331
CDR0601590

;11342
#え、ええ…。
Y-Yes...

;11361
SRK0601610

;11372
#先生は平気ですか？
What about Teacher?

;11400
OPR0605000

;11411
I'm fine.

;11427
CDR0601600

;11438
Hey, Sharo, what happened!?

;11497
CDR0601610

;11508
#大体どうしてみんながここに？
And why is everybody here?!

;11551
SRK0601620

;11562
Eh?!

;11572
SRK0601630

;11583
Well, that's...

;11614
CDR0601620

;11625
CDR0601630

;11636
#まさか…尾行してきたんじゃ…
Don't tell me you were following us?

;11679
SRK0601640

;11690
#ええ！？　いえ、そんな尾行だなんて！
Eh?! No, well, not exactly!

;11745
SRK0601650

;11756
#ただ、ちょっと心配だったから～…
We were just a bit worried～...

;11805
CDR0601640

;11816
#何が心配だったの？
Worried about what?

;11844
SRK0601660

;11855
#コーデリアさん…
#先生とふたりっきりになんかなったら…
You were all alone with Teacher...

;11935
SRK0601670

;11946
#テンパって失敗しちゃうんじゃないかって…
#ネロが～…
Nero feared you might lose your
yourself and make some grave mistake～...

;12023
CDR0601650

;12034
#やっぱり尾行してたのね！？
#どうして！？
So you were tailing us!
Why?!

;12093
CDR0601660

;12104
#みんなで決めたことなのに！
#クジで勝った一人が教官とデートするって！
Even though we had an agreement...
Whoever wins the lottery
goes on a date with Professor...

;12205
SRK0601680

;12216
#あのクジも…実はネロが…
#コーデリアさんに当たりを引かせるように～…
Well in fact, that lottery,
Nero rigged it so you could win～

;12317
NER0601550

;12328
#あ、ちょっと！　それは言わない約束！
Ah, come on! We had an agreement!

;12383
ERI0601510

;12394
#みんな！
Please!

;12407
ALL0600110

;12418
!!

;12425
ERI0601520

;12436
#あ…ごめんなさい…
Ah... Sorry...

;12464
ERI0601530

;12475
#でも…今はそんなことを言ってる場合じゃ…
But, we have bigger problems right now...

;12536
NER0601570

;12547
#そ、そうだね…
#かなりの非常事態だと思う…。
E-Exactly...
We have quite an emergency here.

;12612
ERI0601540

;12623
#うん…電話だって通じないし…。
Yes... The phones can't even connect...

;12669
CDR0601680

;12680
The phones?

;12693
NER0601580

;12704
There's no signal...

;12741
OPR0605010

;12752
The power and the communications are cut off...

;12789
OPR0605020

;12800
At this rate, the whole town will...

;12849
NER0601590

;12860
Hey, Kobayashi...

;12876
OPR0605030

;12887
NER0601600

;12898
The guy from before...
Was it the real Phantom L?

;12960
OPR0605040

;12971
CDR0601690

;12982
Please Nero!
Pay attention to what you are saying!

;13034
CDR0601700

;13045
Because of Phantom L...
Professor's Toys are...

;13107
NER0601610

;13118
Well, that's how it is...

;13158
NER0601620

;13169
The person responsible for
this situation calls himself Phantom L.

;13243
NER0601630

;13254
That's all we got right now.
And only Kobayashi knows L personally.

;13346
CDR0601710

;13357
T-That's... true...

;13397
OPR0605050

;13408
I don't know...

;13427
OPR0605060

;13438
Honestly, I don't know myself...
Is it the real Phantom L or not...

;13524
OPR0605070

;13535
But that sound...
The sound of bells before the broadcast...

;13597
OPR0605080

;13608
The same sound played every time
Phantom L committed a crime...

;13694
NER0601640

;13705
CDR0601730

;13716
#と…
F...

;13723
CDR0601740

;13734
#とりあえず…
For now...

;13753
CDR0601750

;13764
Should we go back to the academy?

;13801
ERI0601560

;13812
The academy?

;13840
CDR0601760

;13851
#ええ…会長がいれば…
#何かわかるかも…。
The chairman might be able to help...

;13910
OPR0605090

;13921
#そうだね。
That's right.

;13937
OPR0605100

;13948
There should also be
an emergency power supply...

;14013
OPR0605110

;14024
And a satellite link as well,
since the academy is part of IDO.

;14083
OPR0605120

;14094
Let's go to the academy!

;14125
ALL0600120

;14136
Yes!

;14149
@ヨコハマ大停電

