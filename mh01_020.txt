;0
__main

;7
@Lclearscreenall

;24
@誤解

;32
SRK0100010

;43
Wow! What nice weather～!

;77
SRK0100020

;88
You can see far away so clearly... so pretty!

;134
SRK0100030

;145
Look Nero! Eri and Cordelia too!

;216
CDR0100010

;227
That's dangerous, Sharo.

;258
NER0100010

;269
What are you getting so excited about...
You can see the scenery anytime, can't you?

;370
SRK0100040

;381
Today is different! It's amazing!

;439
SRK0100050

;450
The "flower" looks really beautiful!

;521
CDR0100020

;532
Flower...?

;551
NER0100020

;562
She's talking about that thing...

;590
NER0100030

;601
The next generation power research plant.

;638
CDR0100030

;649
Ah, the "Fortune Leaf"...

;707
NER0100040

;718
Yeah, Sharo is always saying
that it looks like a flower.

;804
CDR0100040

;815
Well, it does look somewhat like a flower...

;870
NER0100050

;881
The symbol of this town's redevelopment...
A large monument built in the sea...

;964
NER0100060

;975
To be honest...

;985
NER0100070

;996
I don't think it's that great... *munch*

;1045
CDR0100050

;1056
Hey, don't talk while you are eating!

;1114
NER0100080

;1125
It's fine, isn't it?
I can't focus if I don't eat sweet things.

;1217
SRK0100060

;1228
Haaa～... so beautiful～...

;1265
CDR0100060

;1276
You too Sharo, quit staring and let's go.

;1356
CDR0100070

;1367
The president wants to see us.

;1419
NER0100090

;1430
What does she want on a holiday...

;1479
CDR0100080

;1490
I said don't talk while eating!

;1530
NER0100100

;1541
So noisy～...

;1566
CDR0100090

;1577
Geeze...!

;1602
ERI0100010

;1613
――――

;1626
ERI0100020

;1637
Cordelia...?

;1668
CDR0100100

;1679
Yes?

;1689
ERI0100030

;1700
U-Um...

;1716
ERI0100040

;1727
D-Don't... be angry...

;1758
CDR0100110

;1769
Eh!? I-I wasn't!

;1803
ERI0100050

;1814
Please...

;1830
CDR0100120

;1841
Don't cry Eri!

;1869
ERI0100060

;1880
Uu...

;1890
CDR0100130

;1901
I was just warning Nero because of
her bad manners! I'm not angry!

;1999
ERI0100070

;2010
R-Really?

;2026
CDR0100140

;2037
Yes! Really! Really!

;2071
CDR0100150

;2082
Sharo and Nero and you
are all my very important friends!

;2159
CDR0100160

;2170
I wouldn't get angry over something like that!

;2225
ERI0100080

;2236
...

;2246
ERI0100090

;2257
Good...

;2276
CDR0100170

;2287
Ho...

;2300
SRK0100070

;2311
Ah～, it really is beautiful～!

;2354
NER0100110

;2365
You are still at it?

;2399
SRK0100080

;2410
But, but...

;2438
SRK0100090

;2449
Ah!!

;2459
SRK0100100

;2470
It turned!! The flower started turning!!
Wah!! Amazing!!! Amazing～!!!

;2580
SRK0100110

;2591
Eh?

;2598
ALL0100010

;2609
!!!!

;2625
SRK0100120

;2636
Kyaaaaaaaaaaaaaaaa!!!

;2703
............

;2716
OPR0100240

;2727
How should I put it...

;2746
OPR0100250

;2757
It looks like a normal school.

;2800
OPR0100260

;2811
There's no school today so it's pretty quiet...
It doesn't feel like there's anyone around.

;2876
OPR0100280

;2887
Holmes Detective Academy...

;2918
OPR0100295

;2929
An international educational institution
created a few years ago to train detectives...

;3012
OPR0100300

;3023
I feel like I'm out-of-place...

;3072
OPR0100310

;3083
Hm...?

;3093
【An academy was created to train detectives】
～Is this suspicious?～

;3203
TCH0100110

;3214
Mr Kobayashi.

;3227
OPR0100320

;3238
Ah, yes...

;3260
TCH0100120

;3271
Miss Henriette is waiting at that
old school building over there.

;3354
TCH0100130

;3365
I am sure she is looking forward to meet you.

;3460
OPR0100330

;3471
I see...

;3484
OPR0100350

;3495
Um...

;3505
OPR0100360

;3516
Does she really have business with me?
As I said before, I'm not a detective.

;3641
OPR0100370

;3652
Even with that "case" recently,
I just happened to be there by chance...

;3738
TCH0100140

;3749
Hohoho... You don't have to be so modest.

;3795
OPR0100380

;3806
I'm not being modest!

;3843
OPR0100390

;3854
And I only came here
because of what you said...

;3931
TCH0100150

;3942
Yes, if I didn't bring you here,
Miss Henriette would have severly punished me.

;4049
OPR0100400

;4060
OPR0100410

;4071
Anyway, I can't do anything anymore...

;4123
OPR0100420

;4134
!

;4138
TCH0100160

;4149
Oh?

;4159
OPR0100430

;4170
A scream!?

;4186
TCH0100170

;4197
Oh my...

;4219
TCH0100180

;4230
Is this a case for the famous detective?

;4285
OPR0100440

;4296
It's not the time to be saying that!

;4339
TCH0100200

;4350
It's in his nature, isn't it?

;4378
OPR0100450

;4389
Over here!?
Is this where the scream came from!?

;4450
SRK0100130

;4461
Ah! Wa! Oh! Falling! Falling!

;4510
OPR0100460

;4521
Above!?

;4531
OPR0100470

;4542
Uh! Too bright! It's hard to see!

;4600
CDR0100190

;4611
Ugh! Don't let go! Sharo!

;4682
NER0100130

;4693
Hey! Eri, give me a hand!

;4742
ERI0100110

;4753
Y-Y-Yes!

;4778
SRK0100140

;4789
It's over～!!!

;4826
ALL0100020

;4837
Sharo!!!

;4862
SRK0100150

;4873
Kyaaaaaaaa!!!

;4916
OPR0100480

;4927
What!!??

;4946
OPR0100490

;4957
Huh!?

;4967
OPR0100500

;4978
S-So light!?

;4997
SRK0100170

;5008
Huh?

;5027
OPR0100520

;5038
Are you alright?

;5063
SRK0100180

;5074
OPR0100530

;5085
Hello?

;5095
SRK0100190

;5106
Eh!? Ah, yes!

;5134
OPR0100540

;5145
I'm going to put you down, ok?

;5161
SRK0100200

;5172
P-P-Please!

;5206
OPR0100550

;5217
Here you go...

;5236
SRK0100210

;5247
OPR0100560

;5258
Fuu...

;5271
OPR0100570

;5282
Um... Sorry...

;5310
SRK0100230

;5321
No no, it's okay!

;5370
OPR0100580

;5381
But I sure was surprised.
You suddenly falling like that.

;5446
SRK0100240

;5457
I'm sorry! I'm sorry!

;5497
OPR0100590

;5508
Are you a student here?

;5530
SRK0100250

;5541
Yes!

;5551
SRK0100260

;5562
Sherlock Shellingford! 15 years old!

;5626
OPR0100600

;5637
Haha...

;5650
OPR0100610

;5661
Are you hurt at all? Are you really alright?

;5704
SRK0100270

;5715
Yes! Thanks to you...

;5749
CDR0100210

;5760
You suspicious persoooooon!!!!!

;5812
OPR0100620

;5823
CDR0100220

;5834
Get away from Sharooooo!!!!!

;5898
OPR0100640

;5909
Argh!!!!!

;5934
SRK0100280

;5945
CDR0100230-01

;5959
Hah! Hah! Hah!

;5993
CDR0100240

;6004
Sharo!!

;6020
SRK0100290

;6031
Y-Yes!?

;6050
CDR0100250

;6061
Did this person do something to you!?

;6098
SRK0100300

;6109
Ah, well...

;6128
CDR0100260

;6139
He did something, right!!??

;6167
SRK0100310

;6178
No, nothing like that!

;6209
CDR0100270

;6220
He～did～some～thing～right～?!!!

;6281
CDR0100280

;6292
Molester!!!

;6308
CDR0100290

;6319
Pervert!!!

;6335
CDR0100300

;6346
Cri-mi-naaal!!!

;6383
CDR0100310

;6394
Just who are you!!??
Answer me～!!

;6471
SRK0100350

;6482
Um... He's unconscious so...
I don't think he can...

;6534
NER0100150

;6545
Cordelia! Stop stop!

;6594
ERI0100130

;6605
L-Let him... go...

;6639
CDR0100320

;6650
Nero, Eri, what are you saying!?

;6702
CDR0100330

;6713
This man is a molester, a pervert and a criminal
who put his hands on Sharo you know!?

;6799
NER0100160

;6810
That's totally wrong!

;6835
ERI0100140

;6846
This person... saved... Sharo...

;6902
CDR0100340

;6913
Right! He saved Sharo!
There's no way I can forgive...

;6993
CDR0100360

;7004
Eh?

;7014
NER0100170

;7025
You dashed out right away so you didn't see.

;7093
ERI0100150

;7104
He... caught Sharo...

;7156
CDR0100380

;7167
T-That's... a lie, isn't it?

;7204
ERI0100160

;7215
It isn't...

;7249
CDR0100390

;7260
Is that true, Sharo?

;7288
SRK0100360

;7299
Yes.

;7312
SRK0100370

;7323
If he didn't save me,
I could've been really hurt...

;7403
CDR0100410

;7414
Ehhh～!!!???

;7457
@伝説の名探偵

