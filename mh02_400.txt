;0
__main

;7
@Lclearscreenall

;24
@ヒラノ

;35
HSG0200150

;46
Ah, Mr Kobayashi...

;71
OPR0403110

;82
Hey there.

;104
OPR0204690

;115
Hirano, is there something here
that's caught your attention?

;179
HSG0200170

;190
Yes, the cornerstone box.

;239
OPR0204700

;250
Oh, you're sharp, as expected.

;281
OPR0204710

;292
A bronze mirror was found at each of the sites...

;338
OPR0204720

;349
You've noticed too,
that they were in the cornerstone boxes.

;432
HSG0200180

;443
Yes, but that's not
the only thing that's caught my interest.

;517
OPR0204730

;528
Eh?

;535
HSG0200190

;546
The box was destroyed from
the force of the explosion,
and the mirror came flying out from inside...

;629
HSG0200200

;640
That's how it happened right?

;671
OPR0204740

;682
Yeah, probably...

;704
HSG0200210

;715
At all three of the explosion sites?

;752
OPR0204750

;763
!

;767
HSG0200220

;778
I think it's strange that
they were all in the same state.

;846
OPR0204760

;857
You've got a point...

;879
OPR0204770

;890
Hm...?

;900
【The items were all in the same state】
～Is this suspicious?～

;1013
............

;1026
HSG0200230

;1037
Am I thinking too much?

;1068
OPR0204790

;1079
Ah, no, I think you are right.

;1144
HSG0200240

;1155
Well then,
I have some other things I'd like to examine so...

;1219
HSG0200250

;1230
Excuse me.

;1249
OPR0204800

;1260
Sure.

;1273
OPR0204820

;1284
She's definitely a subordinate of Kamitsu...
She's good...

;1343
@カードの秘密

