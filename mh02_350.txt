;0
__main

;7
@Lclearscreenall

;24
@エリーのボーナス

;50
OPR0203910

;61
Hercule.

;83
ERI0200720

;94
Ah, Mr Kobayashi...

;119
OPR0203920

;130
How's it going? Has there been any progress?

;170
ERI0200730

;181
No... not really...

;203
OPR0203930

;214
I see...

;227
............

;240
OPR0203960

;251
Oh no... the conversation's stopped!
I need something to talk about!

;316
ERI0200760

;327
Mr Kobayashi!!

;346
OPR0203970

;357
Yes!?

;370
ERI0200770

;381
Um... D-Do you like books?

;424
OPR0203980

;435
Books...?

;445
ERI0200780

;456
Yes...!

;469
I love them of course.

;497
Enough that I read once in a while.

;528
That's sudden.

;541
OPR0203990

;552
I love them of course.

;583
ERI0200790

;594
Re...Really...!?

;625
OPR0204000

;636
Yeah.

;646
OPR0204010

;657
Enough that I read once in a while...
To be honest, unless I really need too,
I don't open them...

;752
ERI0200800

;763
Is that so...

;785
OPR0204020

;796
But well, I don't hate them.

;836
OPR0204030

;847
That's sudden...

;866
ERI0200810

;877
S...! Sorry...!

;914
OPR0204040

;925
But well, I don't hate them.

;968
OPR0204050

;979
Books make you feel as though
you've been taken to all sorts of places.

;1071
ERI0200820

;1082
!

;1086
OPR0204060

;1097
And, you can become different types of people too.

;1158
ERI0200830

;1169
――――！

;1185
ERI0200840

;1196
I...

;1203
ERI0200850

;1214
I know right!!

;1239
OPR0204070

;1250
ERI0200860

;1261
It's just like that! Books are wonderful!

;1316
OPR0204080

;1327
Y-Yeah, Yeah.

;1352
OPR0204090

;1363
The sparkle in her eyes is extraordinary...

;1403
ERI0200870

;1414
Next time, please let me borrow...
one of your favourites...

;1494
OPR0204100

;1505
Sure...

;1536
ERI0200880

;1547
I'll lend you... one of my favourites too...

;1615
OPR0204110

;1626
Yeah, I'm looking forward to it.

;1660
ERI0200890

;1671
Then I will return to the investigation...

;1705
OPR0204130

;1716
She really likes books...

;1759
@カードの秘密

