;0
__main

;7
@Lclearscreenall

;24
@4エリーのボーナス

;51
OPR0402340

;62
Hercule.

;84
ERI0400500

;95
Ah... Mr Kobayashi...

;120
OPR0402350

;131
It looks like you're really
putting effort into investigating,
don't push yourself too much, though.

;223
ERI0400510

;234
I'm not pushing myself... I like it...

;293
OPR0402360

;304
I see...

;320
ERI0400520

;331
――――

;344
ANR0400170

;355
Hello.

;377
OPR0402370

;388
Eh?

;395
ERI0400530

;406
President...

;419
OPR0402380

;430
Miss Henriette...
Why are you here?

;483
ANR0400180

;494
I happened to be nearby,
so I came to see how you guys were doing...

;565
OPR0201530

;576
Hm...?

;586
【Henriette came to see us】
～Is this suspicious?～

;678
ANR0400190

;689
How is the investigation?

;729
OPR0402390

;740
We are in the middle of it.

;765
ERI0400540

;776
Yes...

;789
ANR0400200

;800
I see... Please do your best, Hercule.

;874
ERI0400550

;885
Y... Yes...!

;904
ANR0400210

;915
Well then, if you'll excuse me.

;943
ERI0400560

;954
ERI0400570

;965
Mr Kobayashi...

;987
OPR0402400

;998
Hm?

;1005
ERI0400580

;1016
The president... is, well,
wonderful, isn't she...?

;1069
ERI0400590

;1080
I-I wonder...

;1096
ERI0400600

;1107
Will I be able to become like her...

;1160
Don't be like that.

;1176
Of course.

;1195
I don't know.

;1211
OPR0402410

;1222
Don't be like that...

;1250
ERI0400610

;1261
Eh...?

;1271
OPR0402420

;1282
You're already charming enough
the way you are now.

;1338
ERI0400620

;1349
!

;1353
ERI0400630

;1364
Ah... I... I'm... that's...

;1404
OPR0402430

;1415
Of course you will.

;1440
ERI0400640

;1451
Are you sure...?

;1479
OPR0402440

;1490
To be honest, I don't know.

;1515
ERI0400650

;1526
I... guess not...

;1554
OPR0402450

;1565
In any case...

;1581
OPR0402460

;1592
I think it's a good thing to think about changing.

;1647
OPR0402470

;1658
If that's what you wish,
you should be able to head somewhere good.

;1729
ERI0400660

;1740
Thank... you...

;1780
OPR0402480

;1791
Ah, no...
It's not like I said anything that great...

;1853
OPR0402490

;1864
Hahaha...

;1880
ERI0400670

;1891
@予告の時間

