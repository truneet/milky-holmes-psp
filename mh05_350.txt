;0
__main

;7
@Lclearscreenall

;24
@5エリーのボーナス

;51
…………

;64
AKT0500850

;75
#ねぇ…ロビー３カメのファイルが
#ないんだけど…。
#(3KAME is a group of electronics shops)
Hey...
I can't find the "3KAME" file on the Desktop...

;146
TYM0500510

;157
#ん？　そんなはず～…。
Hm? It should be there though～

;191
ERI0500760

;202
――――

;215
ZGT0500470

;226
#ごめんごめん、別の場所に移動しちゃってた。
Sorry, I moved it somewhere else.

;290
HSG0500390

;301
#え？　アクセス制限かかってないんですか？
Eh? Isn't it access restricted?

;362
ERI0500770

;373
TYM0500520

;384
#いけね～…全フォルダ…
#フルアクセスになってら～…。
No good～ All folders have
Full Control permissions～

;461
AKT0500860

;472
#フルアクセス！？
#うっかり消しちゃったらどうすんのよ！？
Full control?!
What if they get deleted accidentally?!

;555
OPR0201530

;566
#ん…？
Hm...?

;576
#【ファイルがフルアクセス設定になっていた】
#～この疑問点を覚えますか？～
【File permissions were set to Full Control】
～Is this suspicious?～

;683
ERI0500780

;694
OPR0502940

;705
#エルキュール？
Hercule?

;727
ERI0500790

;738
#！
!

;742
ERI0500800

;753
#こ、小林さん…。
Mr Kobayashi...

;778
OPR0502950

;789
#どうしたんだい？　ソワソワして？
Is something wrong?
You seem nervous.

;838
ERI0500810

;849
#いえ…別に…何でもないです…。
No... It's nothing...

;895
OPR0502960

;906
#？
?

;910
TYM0500530

;921
#ほ～い、直したよ～。
Done～
I've fixed it～

;952
AKT0500870

;963
#しっかりしてよね！
#署からまた持ち出すわけにいかないんだから！
Do it properly! We can't steal
any more data from the police station!

;1055
ERI0500820

;1066
TYM0500540

;1077
#あ、飲み物なくなった～…。
Ah, my glass is empty～...

;1117
AKT0500880

;1128
#ちょっと聞いてるの！？
Did you hear me?!

;1162
ERI0500830

;1173
OPR0502970

;1184
#ん…ひょっとして…
Hm... This atmosphere...

;1212
#とまどってたりする？
Are you surprised?

;1243
#さわがしいかな？
Is it too noisy?

;1268
#楽しいのかな？
Are you enjoying it?

;1290
OPR0502980

;1301
OPR0502990

;1312
#にぎやかすぎて？
Too lively maybe?

;1337
ERI0500850

;1348
#いえ…そんなこと…。
No... That's not it...

;1379
OPR0503000

;1390
#僕もだよ。
I'm the same.

;1406
ERI0500860

;1417
#え？
Eh?

;1424
OPR0503010

;1435
#ここにこんなに人が集まるなんて
#初めてだからさ。
It's my first time seeing
so many people gathered here.

;1506
OPR0503020

;1517
#僕もちょっととまどってる。
I was kind of surprised too.

;1557
ERI0500870

;1568
#小林さんも…。
Mr Kobayashi too...

;1590
OPR0503030

;1601
#よし、それじゃ…
Well then...

;1626
OPR0503040

;1637
OPR0503050

;1648
#ここにこんなに人が集まるなんて
#初めてだし…
Since it's the first time
we have so many guests here...

;1713
ERI0500880

;1724
#い…いえ…
#そんなことないです…。
N...no...
That's not it...

;1774
OPR0503060

;1785
#そんなことあるな…。
Looks like it is...

;1816
OPR0503070

;1827
OPR0503080

;1838
OPR0503090

;1849
#人がたくさん集まってにぎやかだから。
Because of the lively ambience.

;1904
ERI0500890

;1915
#違います…。
It's not like that...

;1934
OPR0503100

;1945
#あれ？
Really?

;1955
OPR0503110

;1966
#よ、よし、それじゃ…
O..Okay, then...

;1997
OPR0503120

;2008
#エルキュール、立って。
Stand up, Hercule.

;2042
ERI0500900

;2053
#はい…？
Sorry?

;2066
OPR0503130

;2077
#気分転換だよ、外の空気を吸うといい。
Let's go outside for a change of pace.

;2132
ERI0500910

;2143
#気分転換…。
Change of pace...

;2162
OPR0503140

;2173
#うん、そんなんじゃ煮詰まるぞ。
Yes, as simple as that.

;2219
ERI0500920

;2230
ERI0500930

;2241
#そう…ですね…
I see...

;2263
ERI0500940

;2274
#あ…！
Ah...!

;2284
OPR0503150

;2295
#おっと！
Careful!

;2308
ERI0500950

;2319
#！

;2323
OPR0503160

;2334
#大丈夫かい、エルキュール？
Are you all right, Hercule?

;2374
ERI0500960

;2385
#は、はい…。
Y-Yes...

;2404
OPR0503170

;2415
#ごめん…僕が急に立てなんて言ったから…。
Sorry... It's my fault for
asking you to stand up so suddenly...

;2476
ERI0500970

;2487
#いえ…そんな事…。
No...Don't worry...

;2515
OPR0503180

;2526
ERI0500980

;2537
CDR0500660

;2548
#ごっほん！！
Ahem!!

;2567
OPR0503190

;2578
#わわっと！
Wa!

;2594
ZGT0500480

;2605
#見た見た？
Did you see that?

;2621
HSG0500400

;2632
#はい、しっかり。
Yes, everything.

;2657
TYM0500550

;2668
#ひゅ～ひゅ～。
Well～ Well～

;2690
AKT0500890

;2701
#何よ…小衣だっていつか警視に…。
So what? Me and Chief will someday...

;2750
CDR0500670

;2761
#教官！　私、いつも言ってますよね！
Professor! Didn't I already tell you?!

;2813
OPR0503200

;2824
#いや…、今のはとっさの事で…
No, I mean... It's just...

;2867
OPR0503210

;2878
#エルキュール！？
#ほ、本当に、大丈夫！？
Hercule?!
Are you sure you are all right?!

;2937
ERI0500990

;2948
#はい…。
Yes...

;2961
OPR0503220

;2972
#よかった、それじゃ、外に…
Good, let's go outside then...

;3012
ERI0501000

;3023
#いえ、捜査にもどります…
#気合いが入ったので…。
No, I should continue the investigation...
I got my fighting spirit back...

;3094
OPR0503230

;3105
#き、気合…？
Fighting spirit...?

;3124
@捜査の後

