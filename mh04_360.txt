;0
__main

;7
@Lclearscreenall

;24
@4コーデリアのキーヒント

;60
OPR0402500

;71
What are you looking at?

;102
CDR0400520

;113
Ah, Professor...

;129
CDR0400530

;140
Using my special investigative authority
I received access to
the building's security data...

;253
OPR0402510

;264
So what data are you looking at then?

;325
CDR0400540

;336
There's only 1 elevator that comes to this floor,
so I was looking at today's records.

;437
OPR0402520

;448
The elevator's records, heh...
The surveillance camera?

;497
CDR0400550

;508
No, the number of people getting on and off...

;548
OPR0402530

;559
Haha, that's an interesting spot
to pay attention to.

;602
CDR0400560

;613
Huh? D-Do you think it's weird...?

;659
OPR0402540

;670
Not at all, think about all possibilities.
It's the basics of investigating,
it's not weird at all.

;768
CDR0400570

;779
Thank you...

;816
OPR0402550

;827
So was there anything you noticed?

;876
CDR0400580

;887
Well... It's all just numbers,
so nothing at the moment...

;939
CDR0400590

;950
Do you notice anything, Professor?

;993
OPR0402560

;1004
Let's take a look...

;1023
CDR0400600

;1034
From left to right is,
time, floor and number of people.
In red is when it went up, and blue is down.

;1144
CDR0400610

;1155
It makes a record when the doors close.

;1210
OPR0402570

;1221
I see...

;1240
CDR0400650

;1251
Um, about that time you just showed me...
I didn't find any problem with it...

;1382
OPR0402580

;1393
I see... Then...

;1424
4003

;1429
CDR0400620

;1440
Alright. I'll look into that time entry.

;1517
OPR0402590

;1528
These 2 lines...

;1547
CDR0400660

;1558
Yes?

;1568
OPR0402600

;1579
Aren't they weird?

;1601
CDR0400670

;1612
16:15:32, 70th floor, 1 person...
16:15:40, 70th floor, 1 person...

;1690
CDR0400680

;1701
The 70th floor is this floor right...
What about it?

;1761
OPR0402610

;1772
It doesn't have a colour
for whether it went up or down...

;1821
OPR0402620

;1832
Wouldn't this mean that the person went in
and without moving to another floor,
came right back out?

;1933
CDR0400690

;1944
I think so...
But they could've just realised they'd
forgotten something, and came right back out...

;2036
OPR0402630

;2047
Something about it is fishy...

;2069
OPR0402640

;2080
The camera...

;2096
OPR0402650

;2107
Can you get an image from
the inside of the elevator, from that time entry?

;2187
CDR0400700

;2198
Ah, there isn't one...

;2223
CDR0400710

;2234
It's see through,
so there is no camera on the inside.

;2308
OPR0402660

;2319
Then check the entry to this floor.
The camera in front of the elevator.

;2414
CDR0400720

;2425
Y-Yes!

;2441
CDR0400730

;2452
This is the image
from when they went in the elevator.

;2489
OPR0402670

;2500
It's Miss Takarada.

;2522
CDR0400740

;2533
And this is from when she came out.

;2567
ALL0400070

;2578
!

;2582
CDR0400760

;2593
H-How can this be.?

;2615
OPR0402690

;2626
This is an important clue...

;2669
@予告の時間

