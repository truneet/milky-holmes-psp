;0
__main

;7
@Lclearscreenall

;24
@結成ミルキィホームズ２

;59
SRK0101740

;70
Wah!!

;83
ERI0101080

;94
So nice...

;110
NER0101650

;121
Very chic...

;143
CDR0101920

;154
I wonder what era is this furniture from...

;194
OPR0103890

;205
――――

;218
ANR0100480

;229
I did it in what I imagined a great detective's
private quarters would look like.
I thought you'd like this kind of style.

;330
SRK0101750

;341
Is that right?

;366
OPR0103900

;377
Y-Yeah...

;393
OPR0103910

;404
Wait, never mind that!

;450
OPR0103920

;461
Even if you prepare this... I...

;513
ANR0100490

;524
Excuse me～

;537
ANR0100500

;548
From what I've heard,
you seem to be having troubles
finding a place to live.

;600
OPR0103930

;611
Ugh...!

;621
ANR0100510

;632
Is that not somewhat of a problem for you?
Please, go ahead and use this room.

;715
SRK0101760

;726
Eh!? Mr Kobayashi, you're going to live here!?

;787
OPR0103940

;798
No! That's just...

;826
SRK0101770

;837
Then, it's our team's room!

;886
SRK0101780

;897
Er... Not a room but...

;934
SRK0101790

;945
That's right! We're detectives, so it's an office!

;991
SRK0101800

;1002
Is it alright for us to use this as our office!?

;1057
ANR0100520

;1068
Of course,
that's what I had planned from the beginning.

;1117
ANR0100530

;1128
And what's more...

;1153
ANR0100540

;1164
Working together with someone who was
once a great detective, should be,

;1265
ANR0100550

;1276
a very great experience for you all.

;1328
SRK0101810

;1339
Wah!!

;1349
CDR0101930

;1360
U-Um... Together with...

;1394
ERI0101090

;1405
Mr Kobayashi...

;1421
NER0101660

;1432
Seems like it might be interesting...

;1478
ANR0100560

;1489
There you have it, Mr Kobayashi.

;1526
ANR0100570

;1537
I intended to welcome you here,
with such a perfect preparation.

;1626
ANR0100580

;1637
Please, consider giving a favourable answer...

;1674
OPR0103950

;1685
L-Like I said, even if you say that...

;1728
SRK0101820

;1739
Please!! Teacher!!

;1785
OPR0103960

;1796
Teacher!?

;1809
SRK0101830

;1820
I want to learn lots of things from you
and quickly become a full fledged detective!

;1918
OPR0103970

;1929
Please, wait!

;1960
NER0101670

;1971
You don't need to think too much about it, do you?

;2029
OPR0103980

;2040
Hey, don't act like it has nothing to do with you!

;2083
ERI0101100

;2094
I... I'd like... to be with... Mr Kobayashi too...

;2143
OPR0103990

;2154
Come on!

;2170
CDR0101940

;2181
Please!! Professor!!

;2233
OPR0104000

;2244
P-Professor!?

;2269
SRK0101840

;2280
Please!!!

;2308
ERI0101110

;2319
P-Please...

;2347
CDR0101950

;2358
NER0101680

;2369
Please～

;2394
ANR0100590

;2405
I insist, Mr Kobayashi.

;2439
OPR0104010

;2450
Ugh...!

;2460
............

;2473
OPR0104030

;2484
I...

;2491
OPR0104040

;2502
I've lost my Toy...

;2533
OPR0104050

;2544
And I stopped being a detective...

;2569
OPR0104060

;2580
I'm really a powerless person...

;2623
OPR0104070

;2634
And yet...

;2650
OPR0104080

;2661
And yet, you want me to...?

;2698
ALL0100190

;2709
Yes!!!!!

;2731
OPR0104100

;2742
――――

;2758
OPR0104110

;2769
To think things would turn out like this...

;2809
OPR0104120

;2820
That's unfair, Miss Henriette...

;2872
ANR0100600

;2883
So is it alright for me
to take your answer as a yes, then?

;2951
OPR0104140

;2962
Yes.

;2972
ALL0100200

;2983
Alright～!!!!!

;3011
ANR0100610

;3022
Thank you, Mr Kobayashi.

;3071
ANR0100620

;3082
Now then, take this...

;3107
OPR0104150

;3118
This is... my old detective licence?

;3167
ANR0100630

;3178
Yes.

;3188
ANR0100640

;3199
If you're going to do detective work,
then mobility is a necessity...
I had it reinstated through some special channels.

;3321
ANR0100650

;3332
Please choose a car you like,
without worrying about any budget...

;3403
ANR0100660

;3414
There isn't a problem, even if it's a classic car.

;3472
OPR0104160

;3483
!!!

;3493
OPR0104170

;3504
That has to be issued from the Ministry of Land,
Infrastructure, Tourism, and Transport...

;3538
OPR0104180

;3549
An internal combustion engine driving permit,
and a right to emit carbon dioxide certificate.

;3617
ANR0100670

;3628
You're fine up to 4 litres.

;3666
OPR0104190

;3677
I...

;3684
OPR0104200

;3695
I can't win against you...

;3720
ANR0100680

;3731
Uhuhuhu

;3750
SRK0101880

;3761
Hey! Hey!

;3783
ALL0100210

;3794
?

;3798
SRK0101890

;3809
Let's all take a picture together!?
In memory of the team's creation!

;3883
ERI0101160

;3894
Eh...!

;3904
SRK0101900

;3915
Don't get shy at times like this! Eri!

;3998
ERI0101170

;4009
Y-Yes...

;4028
SRK0101910

;4039
Cordelia too, come on, come on!

;4085
CDR0102000

;4096
I guess it can't be helped... geeze...

;4133
SRK0101920

;4144
Nero, you come too!

;4169
NER0101730

;4180
Wait a sec. I found a cake in the fridge.

;4251
SRK0101930

;4262
Teacher too!!

;4284
OPR0104210

;4295
Ehh!? Me too!?

;4329
SRK0101940

;4340
Of course! You're our teacher from now on!

;4435
OPR0104220

;4446
I give up...

;4465
ANR0100690

;4476
Alright then, I will take the picture.

;4540
SRK0101950

;4551
Ah, sorry! Please do!

;4600
NER0101740

;4611
No wonder this cake is good, it's from Nuhachi...

;4675
ANR0100710

;4686
Nero, you want to appear on the photo like that?

;4726
NER0101750

;4737
It's fine, I don't mind.

;4765
ANR0100720

;4776
Hercule, you're face is red.

;4825
ERI0101180

;4836
S-Sorry...!

;4867
ANR0100730

;4878
Uhuhu, alright, here I go.

;4912
SRK0101960

;4923
Ah!!!

;4936
ANR0100740

;4947
What is it?

;4969
SRK0101970

;4980
I... just got an idea!

;5020
NER0101760

;5031
What idea?

;5041
SRK0101980

;5052
Listen, Nero! Cordelia and Eri too!

;5126
SRK0101990

;5137
What is it?

;5153
NER0101780

;5164
What do you mean?

;5189
SRK0102000

;5200
It's our name!

;5234
ERI0101200

;5245
Name?

;5258
SRK0102010

;5269
Right! Name!

;5291
SRK0102020

;5302
We just became a team after all!

;5351
CDR0102030

;5362
You're right, that might be good.

;5405
SRK0102030

;5416
Alright then, let's announce it out loud～!

;5456
ERI0101210

;5467
Eh...!?

;5480
NER0101790

;5491
Now!?

;5504
CDR0102040

;5515
Wait, wait!

;5546
SRK0102040

;5557
If we yell with a smile,
it'll make for a good picture!

;5609
SRK0102050

;5620
Ready～!

;5633
SRK0102060

;5644
Our name is!!!

;5684
ALL0100220

;5695
Milky Holmes!

;5723
MovieChapterEnd

;5739
@G4

