;0
__main

;7
@Lclearscreenall

;24
@ツギコ

;35
ZGT0200140

;46
Here we go!

;68
OPR0204830

;79
Tsugiko... What is she doing?

;122
ZGT0200150

;133
Ummm, what does it say...

;161
OPR0204840

;172
That's a fortune cookie.
A snack that has a fortune inside it.

;270
............

;283
OPR0204850

;294
Hey, taking a break?

;319
ZGT0200170

;330
Woah!

;346
OPR0204860

;357
Uwa!

;373
ZGT0200190

;384
N-No way... you're... m-my...

;439
OPR0204880

;450
Eh?

;457
ZGT0200200

;468
As if that could be～!
Ehehehe～!

;517
OPR0204890

;528
What???

;544
ZGT0200210

;555
See ya!

;568
OPR0204900

;579
Ah, you dropped it! Your fortune!

;622
ZGT0200220

;633
You can have it～!

;661
OPR0204920

;672
What was written on it?

;709
OPR0204930

;720
Hmm...

;733
OPR0204940

;744
Your soulmate is before your eyes...

;784
OPR0204950

;795
The two of you are destined to be together,
and have a peaceful household, thriving business;

;875
OPR0204960

;886
prospering descendents, a peaceful death,
abundance of food and drink, pandemonium...?

;950
OPR0204970

;961
――――

;974
@カードの秘密

