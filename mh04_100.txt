;0
__main

;7
@Lclearscreenall

;24
@４話後日談

;41
OPR0401390

;52
After that...

;68
OPR0401400

;79
The phantom thief Twenty escaped
and didn't come back for the Adam's Tear.

;180
OPR0401410

;191
To accept your loss when your plans fail.
Perhaps that's a phantom thief's virtue.

;304
OPR0401420

;315
During the case, the real Kamitsu
was on a plane heading overseas.

;407
OPR0401430

;418
Apparently when he arrived,
he learned what happened
and immediately returned here.

;519
OPR0401440

;530
Was his business trip a coincidence?

;576
OPR0401450

;587
Or was it...

;603
NER0400370

;614
We're back～

;636
SRK0400570

;647
It was good～!

;672
CDR0400470

;683
Professor, thank you.

;726
ERI0400310

;737
Thank you for the meal...

;771
OPR0401470

;782
How was the cake buffet?

;831
CDR0400480

;842
It was really great...
There were many different cakes.

;916
SRK0400580

;927
On top of conquering all the buffet,
Nero even had Matsusaka steak!

;1007
OPR0401480

;1018
Eh? Steak?

;1043
CDR0400490

;1054
Sharo

;1067
SRK0400590

;1078
Ah!

;1088
OPR0401490

;1099
Um...

;1109
OPR0401500

;1120
I know I said I'd treat you to something,

;1185
OPR0401510

;1196
as a reward for solving the case without incident.

;1270
OPR0401520

;1281
But that all-you-can-eat
cake buffet was all you wanted?

;1361
CDR0400500

;1372
Well that's... Um... because Nero went and... 

;1418
NER0400380

;1429
Ah～ That's mean. Cordelia～

;1472
NER0400390

;1483
You ordered something too didn't you.
Foie gras saute.

;1557
OPR0401530

;1568
Foie gras!?

;1590
CDR0400510

;1601
Ah! No! Um!

;1632
OPR0401540

;1643
Don't tell me you guys did too, Sherlock!? Hercule!?

;1702
SRK0400600

;1713
Ehehehe...

;1735
ERI0400320

;1746
Sorry...

;1768
OPR0401550

;1779
Ah... you apologising means that...

;1807
NER0400400

;1818
Sharo had the truffle cream fettuccine.

;1879
NER0400410

;1890
Eri had crackers topped with top-class caviare.

;1948
OPR0401560

;1959
Truffle and caviare!!?

;1999
NER0400420

;2010
Here's the receipt.

;2032
OPR0401570

;2043
!!!!!

;2059
NER0400430

;2070
Classy hotels really are something else aren't they!

;2135
OPR0401580

;2146
Um... the cost...
our expenses wont be able to cover it...

;2211
MovieChapterEnd

;2227
14004

;2233
1

;2235
@ラット登場

;2252
@ストーンリバー登場

;2281
@アルセーヌとスリーカード

